<?php
	include_once ('ejercicio_1_config.php');

	$consulta = pg_query($conn, "SELECT p.producto_id, p.nombre, p.descripcion, m.nombre, t.nombre FROM producto p, marca m,tipo t WHERE p.marca_id = m.marca_id AND p.tipo_id = t.tipo_id ORDER BY p.producto_id");

	if (!$consulta) {
		echo "Ocurrió un error al consultar";
		exit;
	} else {
		echo "<div><b> LISTADO DE PRODUCTOS </b></div>";
		echo "<table>"; //Tabla con Productos
		echo "<tr>";
			echo "<td style='border:1px solid black; background-color:red;'> Nro. Producto </td>";
			echo "<td style='border:1px solid black; background-color:red;'> Nombre </td>";
			echo "<td style='border:1px solid black; background-color:red;'> Descripcion </td>";
			echo "<td style='border:1px solid black; background-color:red;'> Marca </td>";
			echo "<td style='border:1px solid black; background-color:red;'> Tipo </td>";
			echo "<td style='border:1px solid black; background-color:red;'> ¿Borrar? </td>";
			echo "<td style='border:1px solid black; background-color:red;'> ¿Editar? </td>";
		echo "</tr>";

		while ($row = pg_fetch_row($consulta)) {
			echo "<tr>";
				echo "<td style='border:1px solid black;'> $row[0] </td>";
				echo "<td style='border:1px solid black;'> $row[1] </td>";
				echo "<td style='border:1px solid black;'> $row[2] </td>";
				echo "<td style='border:1px solid black;'> $row[3] </td>";
				echo "<td style='border:1px solid black;'> $row[4] </td>";
				echo "<td style='border:1px solid black;'> <a href='ej5_1_borrar.php?id=$row[0]'> Borrar </a> </td>";
				echo "<td style='border:1px solid black;'> <a href='ej5_1_editar.php?id=$row[0]'> Editar </a> </td>";
			echo "</tr>";
		}
		echo "</table>";
		echo "<a href='ejercicio_1_insertar.php'>Insertar Producto</a>";
	}
?>