<?php
	// Crea la Conexion
	include_once ('ejercicio_1_config.php');

	if (!$conn) {
		echo "Ocurrió un error al conectar";
		exit;
	} else {
		/*Cadena Heredoc, permite expandir variables en PHP*/
		$str=<<<HTML
			<form action="#" method="post">
				<div> PANTALLA DE LOGIN </div>
				<div>
					<label for="usuario">Usuario:</label>
					<input type="text" name="usuario" placeholder="Introduzca su usuario" />
				</div>
				<div>
					<label for="password">Password:</label>
					<input type="password" name="password" placeholder="Introduzca su password" />
				</div>
				<br/>
				<div class="button">
					<button type="submit">Iniciar Sesión...</button>
				</div>
			</form>
	HTML;

		if (!isset($_POST['usuario']) && !isset($_POST['password'])) {
			echo $str; //Imprimo el formulario cuando no me llega información por Post
		} else {
			$usuario = $_POST['usuario'];
			$password = $_POST['password'];
			
			// Se hace el select con al crypt para verificar si la contraseña esta correcta.
			$resultado= pg_query($conn, "SELECT * from usuarios WHERE usuario = '$usuario' AND password = crypt('$password', password)");

			if (!$resultado) {
				echo "Ocurrió un error al consultar";
				exit;
			} else {
				// Se verifica si hubo 1 registro afectado (login correcto). Mostrar registros.
				if (pg_affected_rows($resultado) == 1) {
					header("Location: ejercicio_1_mostrar.php");
				} else {
					echo "Se ingresó un usuario y/o contraseña erronea.<br>";
					echo "<a href='ejercicio_1_login.php'> Volver a intentar </a>";
				}
			}
		}
	}
?>