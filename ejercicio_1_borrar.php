<?php
	// Crea la Conexion
	include_once ('ejercicio_1_config.php');

	if (isset($_GET['id']) && !empty($_GET['id'])) {
		// Get hidden input value
		$id = $_GET["id"];

		$resultado= pg_query($conn, "DELETE FROM producto WHERE producto_id = $id");

		if (!$resultado) {
			echo "Ocurrió un error al consultar";
			exit;
		} else {
			// Se verifica si hubo 1 registro afectado (login correcto). Mostrar registros.
			if (pg_affected_rows($resultado) == 1) {
				header("Location: ejercicio_1_mostrar.php");
			} else {
				echo "ERROR al Borrar.<br>";
			}
		}

	} else {
		echo "Error al intentar borrar un producto.";
	}
?>