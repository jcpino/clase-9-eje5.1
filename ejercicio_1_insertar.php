<?php
	// Crea la Conexion
	include_once ('ejercicio_1_config.php');

	$marcas = pg_query($conn, "SELECT * FROM marca");
	$tipos = pg_query($conn, "SELECT * FROM tipo");

	$str=<<<HTML
	<form action="#" method="post">
			<div><b> INSERTAR PRODUCTO </b></div>
			<div> 
				<label for="nombre">Nombre del Producto:</label>
				<input type="text" name="nombre" placeholder="" />
			</div>
			<div> 
				<label for="descripcion">Descripción:</label>
				<input type="text" name="descripcion" placeholder="" />
			</div>
			<div> 
				<label for="marca">Marca:</label>
				<select id="marca" name="marca">
	HTML;
				while ($row = pg_fetch_row($marcas)) {
					$str .= "<option value=' " . $row['0'] . "'> ". $row['1'] . "</option>";
				}
	$str.=<<<HTML
				</select>
			</div>
			<div> 
				<label for="tipo">Tipo:</label>
				<select id="tipo" name="tipo">
	HTML;
				while ($row = pg_fetch_row($tipos)) {
					$str .= "<option value=' " . $row['0'] . "'> ". $row['1'] . "</option>";
				}
	$str.=<<<HTML
				</select>
			</div>
			<br/>
			<div class="button">
				<button type="submit">Guardar Cambios</button>
			</div>
		</form>
	HTML;

	if (!isset($_POST['nombre']) && !isset($_POST['descripcion'])) {
		echo $str; //Imprimo el formulario cuando no me llega información por Post
	} else {
		$tipo = $_POST['tipo'];
		$marca = $_POST['marca'];
		$nombre = $_POST['nombre'];
		$descripcion = $_POST['descripcion'];
		
		// Obtener el ultimo producto_id
		$resultado_product_id = pg_query($conn, "SELECT producto_id FROM producto ORDER BY producto_id DESC LIMIT 1");
		$ultimo_product_id = pg_fetch_row($resultado_product_id);
		$ultimo_product_id = $ultimo_product_id[0] + 1;

		// Se hace el select con al crypt para verificar si la contraseña esta correcta.
		$resultado= pg_query($conn, "INSERT INTO producto VALUES ($ultimo_product_id, $tipo, $marca, '$nombre', '$descripcion')");

		if (!$resultado) {
			echo "Ocurrió un error al consultar";
			exit;
		} else {
			// Se verifica si hubo 1 registro afectado (login correcto). Mostrar registros.
			if (pg_affected_rows($resultado) == 1) {
				header("Location: ejercicio_1_mostrar.php");
			} else {
				echo "ERROR al Insertar.<br>";
			}
		}
	}
?>